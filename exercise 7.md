# Coursework 2 Project Planning exercise

Planning is the keystone of successful programming, but it need not all be dry paperwork.  The idea of experimenting with code in a series of feasibility studies can really help identify which approach to take.

In this exercise, you have some thinking to do, and you should get together with others in your peer group (other students) to discuss ideas.

First, though, grab a copy of this project by going to it on CSGitLab and using the "Fork" button near the top right corner.

![image.png](./image.png)

Please set your version of the project to "private" after forking.

## Goals 

Identify the key goals you need to complete for the coursework

1. One goal i am aspiring to achieve is to be able to follow numerous instructions and be able to keep the evidece of completion within a CSGitlab project.
1. Another key goal is to be able to use a gantt chart to manage my time wisely and improve my time management skills. 
1. Another key goal in order to complete coursework is to learn new skills as well as use new software in order to achieve completion of the intructions.
1. Another key goal to achieve the completion for coursework involves ability to use terminal for C+ as well as other langauges to be able to command for he desired outcome to be achieved.
1. As well as this, to be able to comment on all actions within the instructions to have a greater understanding of whas being achieved. 


## Requirements to fulfill goals

What do you need to be able to meet those goals?  This can include clearer specifications, tests (what needs testing, how to test it?), knowledge etc.

1. Requirements to be able to complete this coursework will involve the extension and increasing my knowledge on new software i am unfamiliar with to be able to achieve the specification.
1. As well as this, another requirement includes the use of time managment skills to allow for the work to be completed effciently and the quality to be at its highests as opposed to rushing the work affecting its quality. 
1. To fullfill my goal i will need to conduct research to make sure the specfications are alot clearer and that i understand what is being expected of me and i can complete the specification correctly as opposed to affecting my time management doing a task incorrectly.
1. Another requirement involves testing of each specification to ensure that the task i had just completed is working. Some tasks such as checking the exsisting file systems for any matching names. 
1. Another requirement for me to fullfil the goal is the abilit to edit my csgitlab to be able to create comments on each task i am completing to be able to keep track of what has been completed, how it has been completed as well as the realistic time fram to update my gantt chart with. 

## Dependencies (mapping goals/requirements etc)

Which requirements relate to which goals - think about drawing out a Product Breakdown for the coursework

1. the requirement of increading my kowledge will especially realte to using a plantuml as i am inexperianced using ths ol and therefore will complete research and increase my knowledge on how to achieve the specfication using plantuml. This will be achieved via online research and help from peers. This will relate to the goals such as learning new skils. 
1. Time management skills will relate to the specif goal of making a gantt chart to predict and track the progress i will be making to breakdown the coursework and understand what tasks may take longer than others. 
1. TO be able to make the specificatin clearer this will relate to all goals to be able to commenting on all the tasks i complete since if the specification is unclear my notes will als be unreliable as well as not as clear as desired. 
1. 
1.

_**Hint**_: This might look something like this:

```mermaid
graph LR

    
    Project --> 1_must_haves
    subgraph "Must have"
    1_must_haves --> 1_create_folder_structure
    1_must_haves --> 2_abort_if_exists
    1_must_haves --> 3_initialise_git
    1_must_haves --> 4_feature_management
    end
    
    4_feature_management --> manage_features
    subgraph "manage features"
        manage_features(Manage Features)
        manage_features --> 1_shorthand_codes
        manage_features --> 2_shorthand_lookup
        manage_features --> 3_git_branch_per_feature
    end
 
    Project --> 2_should_haves
    subgraph "Should have"
    2_should_haves --> 5_renaming_features
    2_should_haves --> 6_moving_features
    2_should_haves --> 7_tree_diagram
    2_should_haves --> 8_time_workload_data
    2_should_haves --> 9_time_workload_on_diagram
    end
    
    Project --> 3_could_haves
    Project --> 4_wont_haves
    Project --> readme.md
    

```

